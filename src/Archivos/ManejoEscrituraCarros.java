package Archivos;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ManejoEscrituraCarros {
    private BufferedWriter escritor;

    public void abrirArchivo() {
        try {
            escritor = new BufferedWriter(new FileWriter("carros.txt"));
        } catch (IOException e) {
            System.err.println("Hubo un error al crear el archivo");
            e.printStackTrace();
        }
    }

    public void escribirArchivo(Carro carro) {
        try {
          escritor.write(carro.toStringArchivo() + "\n");
        } catch (IOException e) {
            System.err.print("Hubo un error al escribir en el archivo");
            e.printStackTrace();
            cerrarArchivo();
        }
    }

    public void cerrarArchivo() {
        try {
            if (escritor != null) {
                escritor.close();
            }
        } catch (IOException e) {
            System.err.println("Hubo un error al cerrar el archivo");
            e.printStackTrace();
        }
    }



}
