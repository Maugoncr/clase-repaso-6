package Archivos;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ManejoLecturaCarros {
    BufferedReader lector;

    public void abrirArchivo() {
        try {
            lector = new BufferedReader(new FileReader("carros.txt"));
        } catch (FileNotFoundException e) {
            System.err.println("Hubo un error al abrir el archivo");
            e.printStackTrace();
        }
    }

    public Carro[] leerArchivo() {
        Carro[] vectorCarros = new Carro[getLineasArchivo()];
        Carro carro;

        try {
            lector = new BufferedReader(new FileReader("carros.txt"));
            String linea = lector.readLine();
            String[] vectorDatos;
            int indice;
            int contador = 0;

            while(linea != null) {
                carro = new Carro();
                indice = 0;
                vectorDatos = linea.split("@");
                carro.setPlaca(vectorDatos[indice++]);
                carro.setMarca(vectorDatos[indice++]);
                carro.setModelo(vectorDatos[indice++]);
                carro.setAnio(Integer.parseInt(vectorDatos[indice]));

                vectorCarros[contador] = carro;
                contador++;
                linea = lector.readLine();
            }

        } catch (IOException e) {
            System.err.println("Hubo un error al leer el archivo");
            e.printStackTrace();
        }

        return vectorCarros;
    }

    public int getLineasArchivo() {
        int cantidadLineas = 0;

        try {
            String linea = lector.readLine();
            while (linea != null) {
                cantidadLineas++;
                linea = lector.readLine();
            }
        } catch (IOException e) {
            System.err.println("Hubo un error al leer el archivo");
            e.printStackTrace();
        }

        return cantidadLineas;
    }

    public void cerrarArchivo() {
        try {
            if(lector != null) {
                lector.close();
            }
        } catch (IOException e) {
            System.err.println("Hubo un error al cerrar el archivo");
            e.printStackTrace();
        }
    }

}
